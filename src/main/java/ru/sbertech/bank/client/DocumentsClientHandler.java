package ru.sbertech.bank.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.Bank;
import ru.sbertech.bank.entities.PaymentDocument;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class DocumentsClientHandler implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(DocumentsClientHandler.class);
    private int codeBank;
    private Bank bank;
    private BlockingQueue<PaymentDocument> documents;

    DocumentsClientHandler(Bank bank) {
        this.documents = new ArrayBlockingQueue<>(100);
        this.bank = bank;
        this.codeBank = bank.getCodeBank();
    }

    private int getCodeBank() {
        return codeBank;
    }

    private BlockingQueue<PaymentDocument> getDocuments() {
        return documents;
    }

    void addDocument(PaymentDocument paymentDocument) {
        getDocuments().add(paymentDocument);
        logger.info("Document was successfully added");
    }

    @Override
    public void run() {
        try {
            handler();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private void handler() throws InterruptedException {
        while (true) {
            PaymentDocument paymentDocument = getDocuments().take();
            transfer(paymentDocument);
        }
    }

    void transfer(PaymentDocument paymentDocument) {
        long creditAccount = paymentDocument.getCreditAccount();
        long debitAccount = paymentDocument.getDebitAccount();
        if (isLocalAccount(creditAccount) && isLocalAccount(debitAccount)) {
            bank.doInternalTransaction(paymentDocument);
        } else {
            addDocForOuterTransaction(paymentDocument);
        }
    }

    private boolean isLocalAccount(long account) {
        int codeBankHolder = getCodeBankHolder(account);
        return codeBankHolder == getCodeBank();
    }

    private void addDocForOuterTransaction(PaymentDocument paymentDocument) {
        ClientTransmitter.addDocument(paymentDocument);
    }

    public static int getCodeBankHolder(long account) {
        long tempNumber = account;
        int numberOfDigits = 0;
        do {
            tempNumber /= 10;
            numberOfDigits++;
        }
        while (tempNumber >= 10);
        int codeBankHolder = 0;
        switch (numberOfDigits) {
            case 6:
            case 8:
                codeBankHolder = (int) Math.floor(account / Math.pow(10, numberOfDigits));
                break;
        }
        return codeBankHolder;
    }
}
