package ru.sbertech.bank.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.PaymentDocument;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClientReceiver implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(ClientReceiver.class);
    private Socket socket;
    private DocumentsClientHandler handler;

    ClientReceiver(Socket socket, DocumentsClientHandler handler) {
        this.socket = socket;
        this.handler = handler;
    }

    @Override
    public void run() {
        try {
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            PaymentDocument taskFromServer;
            while (true) {
                taskFromServer = (PaymentDocument) reader.readObject();
                PaymentDocument paymentDocument = new PaymentDocument(taskFromServer.getCreditAccount(), taskFromServer.getDebitAccount(), taskFromServer.getSum().doubleValue());
                handler.transfer(paymentDocument);
            }

        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }
}
