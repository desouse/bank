package ru.sbertech.bank.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.Bank;
import ru.sbertech.bank.entities.CorrespondBankAccount;
import ru.sbertech.bank.entities.PaymentDocument;
import ru.sbertech.bank.entities.PersonalBankAccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientApplication {
    private final static Logger logger = LoggerFactory.getLogger(ClientApplication.class);
    private Bank bank;
    private ClientTransmitter clientTransmitter;

    public static void main(String[] args) {
        ClientApplication app = new ClientApplication();
        app.startApp();
    }

    private void startApp() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameBank = null;
        int codeBank = 0;
        try {
            System.out.println("Enter bank name");
            nameBank = reader.readLine();
            System.out.println("Enter bank code");
            codeBank = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        bank = new Bank(nameBank, codeBank);
        bank.loadData();
        int size = Bank.getBankAccountList().getAccountConcurrentHashMap().size();
        PersonalBankAccount.setId(size);
        Socket socket = getSocket();

        DocumentsClientHandler documentsClientHandler = new DocumentsClientHandler(bank);
        clientTransmitter = new ClientTransmitter(bank, socket);
        ClientReceiver customerListener = new ClientReceiver(socket, documentsClientHandler);
        new Thread(documentsClientHandler).start();
        Bank.bankAccountList.getListAllAccounts();
        new Thread(clientTransmitter).start();
        new Thread(customerListener).start();


        try {
            runCommand(reader, codeBank, documentsClientHandler);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private Socket getSocket() {
        Socket socket = null;
        try {
            socket = new Socket("localhost", 9000);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return socket;
    }

    private void runCommand(BufferedReader reader, int codeBank, DocumentsClientHandler documentsClientHandler) throws IOException {
        while (true) {
            System.out.println("Enter command: ");
            String operation = reader.readLine();
            operation = operation.toUpperCase();
            switch (operation) {
                case "ALL":
                    showListAllAccounts();
                    break;
                case "ADPA":
                    addPersonalAccount(reader);
                    break;
                case "ADCA":
                    addCorrespondAccount(reader, codeBank);
                    break;
                case "CPD":
                    createPaymentDocument(reader, documentsClientHandler);
                    break;
                case "SAVE":
                    bank.saveData();
                    break;
                case "EXIT":
                    exitCommand();
                    break;
                default:
                    logger.error("This command is not supported!");
            }
        }
    }

    private void createPaymentDocument(BufferedReader reader, DocumentsClientHandler documentsClientHandler) throws IOException {
        System.out.println("Enter data");
        String payDoc = reader.readLine();
        String[] param = payDoc.split(" ");
        long creditAccount = Long.parseLong(param[0]);
        long debitAccount = Long.parseLong(param[1]);
        double amount = Double.parseDouble(param[2]);
        PaymentDocument paymentDocument = new PaymentDocument(creditAccount, debitAccount, amount);
        documentsClientHandler.addDocument(paymentDocument);
    }

    private void showListAllAccounts() {
        Bank.bankAccountList.getListAllAccounts();
        logger.info("Operation successfully done");
    }

    private void addCorrespondAccount(BufferedReader reader, int codeBank) throws IOException {
        System.out.println("Enter code bank for Correspond Account & Balance");
        String s = reader.readLine();
        String sParam[] = s.split(" ");
        int codeBankForCorrespondAccount = Integer.parseInt(sParam[0]);
        double balanceForCorrespondAccount = Double.parseDouble(sParam[1]);
        CorrespondBankAccount correspondAccount = new CorrespondBankAccount(codeBank, codeBankForCorrespondAccount, balanceForCorrespondAccount);
        Bank.bankAccountList.addAccount(correspondAccount);
        clientTransmitter.sendAccountList(correspondAccount);
    }

    private void addPersonalAccount(BufferedReader reader) throws IOException {
        System.out.println("Enter account deposit for new Personal BankAccount");
        String numAccount = reader.readLine();
        double balanceForPersonalAccount = Double.parseDouble(numAccount);
        PersonalBankAccount personalAccount = new PersonalBankAccount(bank.getCodeBank(), balanceForPersonalAccount);
        Bank.bankAccountList.addAccount(personalAccount);
        clientTransmitter.sendAccountList(personalAccount);
    }

    private void exitCommand() {
        bank.saveData();
        System.exit(0);
    }
}
