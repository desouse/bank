package ru.sbertech.bank.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.Bank;
import ru.sbertech.bank.entities.BankAccount;
import ru.sbertech.bank.entities.PaymentDocument;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ClientTransmitter implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(ClientReceiver.class);
    private static BlockingQueue<PaymentDocument> documentQueue = new ArrayBlockingQueue<>(100);
    private Bank bank;
    private Socket socket;
    private ObjectOutputStream objectOutputStream;


    ClientTransmitter(Bank bank, Socket socket) {
        this.bank = bank;
        this.socket = socket;
    }

    static void addDocument(PaymentDocument paymentDocument) {
        documentQueue.add(paymentDocument);
    }

    @Override
    public void run() {
        try {
            logger.info("Connect");
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            authorizationOfBank();
            while (true) {
                PaymentDocument paymentDocument = documentQueue.take();
                transferToServer(paymentDocument);
            }
        } catch (IOException | InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private void authorizationOfBank() throws IOException {
        objectOutputStream.writeObject(bank.getCodeBank());
        objectOutputStream.flush();
        objectOutputStream.writeObject(Bank.bankAccountList);
        objectOutputStream.flush();
    }

    private void sendRequest(PaymentDocument message) throws IOException {
        objectOutputStream.writeObject(message);
        objectOutputStream.flush();
    }

    void sendAccountList(BankAccount bankAccount) throws IOException {
        objectOutputStream.writeObject(bankAccount);
        objectOutputStream.flush();
    }

    private void transferToServer(PaymentDocument paymentDocument) throws InterruptedException, IOException {
        sendRequest(paymentDocument);
    }
}
