package ru.sbertech.bank.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.BankAccountList;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ServerApplication {
    private final static Logger logger = LoggerFactory.getLogger(ServerApplication.class);
    public static Map<Integer, ServerReceiver> banks = new HashMap<>();
    public static BankAccountList account = new BankAccountList();

    public static void main(String[] args) {
        ServerApplication serverApplication = new ServerApplication();
        logger.info("Start working");
        ServerTransmitter serverTransmitter = new ServerTransmitter();
        DocumentsServerHandler documentsServerHandler = new DocumentsServerHandler(serverTransmitter);
        new Thread(documentsServerHandler).start();
        serverApplication.startServer();
    }

    private void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(9000)) {
            while (true) {
                Socket socket = serverSocket.accept();
                ServerReceiver serverReceiver = new ServerReceiver(socket);
                new Thread(serverReceiver).start();
                logger.info("New bank");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void printClients() {
        for (ServerReceiver account : banks.values()) {
            logger.info(account.toString());
        }
    }
}
