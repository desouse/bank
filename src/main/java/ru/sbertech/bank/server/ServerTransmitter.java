package ru.sbertech.bank.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.PaymentDocument;

import java.io.IOException;
import java.io.ObjectOutputStream;

class ServerTransmitter {
    private final static Logger logger = LoggerFactory.getLogger(DocumentsServerHandler.class);
    synchronized void sendNewTaskToClient(int codeBank, PaymentDocument paymentDocument) {
        ServerReceiver client = ServerApplication.banks.get(codeBank);
        transaction(paymentDocument);
        try {
            ObjectOutputStream writer = new ObjectOutputStream(client.getSocket().getOutputStream());
            writer.writeObject(paymentDocument);
            writer.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void transaction(PaymentDocument document) {
        ServerApplication.account.getAccountConcurrentHashMap().get(document.getCreditAccount()).doCreditOperation(document.getSum());
        ServerApplication.account.getAccountConcurrentHashMap().get(document.getDebitAccount()).doDebitOperation(document.getSum());

        ServerApplication.account.getListAllAccounts();
    }
}
