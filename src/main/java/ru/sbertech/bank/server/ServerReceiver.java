package ru.sbertech.bank.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.entities.BankAccount;
import ru.sbertech.bank.entities.BankAccountList;
import ru.sbertech.bank.entities.PaymentDocument;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ServerReceiver implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(ServerReceiver.class);
    private Socket socket;
    private int codeBank;

    ServerReceiver(Socket socket) {
        this.socket = socket;
    }

    private int getCodeBank() {
        return codeBank;
    }

    private void setCodeBank(int codeBank) {
        this.codeBank = codeBank;
    }

    Socket getSocket() {
        return socket;
    }

    private void addMapClientForSelf() {
        ServerApplication.banks.put(getCodeBank(), this);
        ServerApplication.printClients();
    }

    @Override
    public void run() {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            setValidCodeBank(inputStream);
            while (true) {
                Object message = inputStream.readObject();
                if (message instanceof PaymentDocument) {
                    PaymentDocument doc = (PaymentDocument) message;
                    DocumentsServerHandler.documents.add(doc);
                }
                if (message instanceof BankAccount) {
                    BankAccount bankAccountList = (BankAccount) message;
                    ServerApplication.account.addAccount(bankAccountList);
                    ServerApplication.account.getListAllAccounts();
                }
            }
        } catch (IOException e) {
            logger.error("Bank (code: " + codeBank + ") is disconnect");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    private void setValidCodeBank(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
        int temp = (Integer) inputStream.readObject();
        BankAccountList bankAccountList = (BankAccountList) inputStream.readObject();
        setCodeBank(temp);
        addMapClientForSelf();
        ServerApplication.account.addListAccount(bankAccountList.getAccountConcurrentHashMap());
    }

    @Override
    public String toString() {
        return "ServerReceiver {" +
                "codeBank = " + codeBank +
                '}';
    }
}
