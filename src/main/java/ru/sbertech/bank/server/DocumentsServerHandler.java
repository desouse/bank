package ru.sbertech.bank.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbertech.bank.client.DocumentsClientHandler;
import ru.sbertech.bank.entities.PaymentDocument;

import java.math.BigDecimal;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class DocumentsServerHandler implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(DocumentsServerHandler.class);
    static BlockingQueue<PaymentDocument> documents = new ArrayBlockingQueue<>(100);
    private ServerTransmitter serverTransmitter;

    DocumentsServerHandler(ServerTransmitter serverTransmitter) {
        this.serverTransmitter = serverTransmitter;
    }

    @Override
    public void run() {
        PaymentDocument doc;
        try {
            while (true) {
                doc = documents.take();
                int codeBankCreditor = DocumentsClientHandler.getCodeBankHolder(doc.getCreditAccount());
                int codeBankDebtor = DocumentsClientHandler.getCodeBankHolder(doc.getDebitAccount());
                if (codeBankCreditor == codeBankDebtor) {
                    if ((validOperation(doc.getCreditAccount(), doc.getSum())) >= 0) {
                        serverTransmitter.sendNewTaskToClient(codeBankCreditor, doc);
                    } else {
                        logger.error("Error: Not enough money");
                    }
                } else {
                    long corAccCredit = getCorrespondAccount(codeBankCreditor, codeBankDebtor);
                    long corAccDebit = getCorrespondAccount(codeBankDebtor, codeBankCreditor);

                    if (validOperation(doc.getCreditAccount(), doc.getSum()) >= 0 && (validOperation(corAccDebit, doc.getSum()) >= 0)) {
                        serverTransmitter.sendNewTaskToClient(codeBankCreditor, new PaymentDocument(doc.getCreditAccount(), corAccCredit, doc.getSum().doubleValue()));
                        serverTransmitter.sendNewTaskToClient(codeBankDebtor, new PaymentDocument(corAccDebit, doc.getDebitAccount(), doc.getSum().doubleValue()));

                    } else {
                        logger.error("Error: Not enough money");
                    }
                }
            }
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private int validOperation(long account, BigDecimal amount) {
        return ServerApplication.account.getAccountConcurrentHashMap().get(account).getBalance().compareTo(amount);
    }

    private long getCorrespondAccount(int codeBankCreditor, int codeBankDebtor) {
        return (long) (codeBankCreditor * (Math.pow(10, 8))
                + codeBankDebtor * (Math.pow(10, 6)) + 1);
    }
}
