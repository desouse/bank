package ru.sbertech.bank.entities;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaymentDocument implements Serializable {
    private long creditAccount;
    private long debitAccount;
    private BigDecimal sum;

    public PaymentDocument(long creditAccount, long debitAccount, double sum) {
        this.creditAccount = creditAccount;
        this.debitAccount = debitAccount;
        this.sum = new BigDecimal(sum).setScale(2, 5);
    }

    public long getCreditAccount() {
        return creditAccount;
    }

    public long getDebitAccount() {
        return debitAccount;
    }

    public BigDecimal getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return creditAccount + " " + debitAccount + " " + sum;
    }
}
