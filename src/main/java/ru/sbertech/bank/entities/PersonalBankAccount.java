package ru.sbertech.bank.entities;

import java.io.Serializable;

public class PersonalBankAccount extends BankAccount implements Serializable {
    private static int id = 0;

    public PersonalBankAccount(int codeBank, double amount) {
        super("PersonalBankAccount", codeBank, amount);
        int number = ++id;
        this.numberBankAccount = (long) (codeBank * Math.pow(10, 6) + number);
    }

    public static void setId(int id) {
        PersonalBankAccount.id = id;
    }
}
