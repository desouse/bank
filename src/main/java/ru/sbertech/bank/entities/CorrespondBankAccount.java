package ru.sbertech.bank.entities;

import java.io.Serializable;

public class CorrespondBankAccount extends BankAccount implements Serializable {
    private static int id = 0;

    public CorrespondBankAccount(int codeBankHolder, int codeBankCorrespond, double amount) {
        super("CorrespondBankAccount", codeBankHolder, amount);
        int count = ++id;
        this.numberBankAccount = (long) (codeBankHolder * (Math.pow(10, 8)) + codeBankCorrespond * Math.pow(10, 6) + count);
    }
}
