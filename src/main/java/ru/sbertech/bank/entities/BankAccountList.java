package ru.sbertech.bank.entities;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class BankAccountList implements Serializable {
    private final static Logger logger = LoggerFactory.getLogger(BankAccountList.class);
    private ConcurrentHashMap<Long, BankAccount> accountConcurrentHashMap;

    public BankAccountList() {
        this.accountConcurrentHashMap = new ConcurrentHashMap<>();
    }

    public ConcurrentHashMap<Long, BankAccount> getAccountConcurrentHashMap() {
        return accountConcurrentHashMap;
    }

    public void addAccount(BankAccount bankAccount) {
        accountConcurrentHashMap.put(bankAccount.getNumberBankAccount(), bankAccount);
    }

    public void addListAccount(ConcurrentHashMap<Long, BankAccount> accountList) {
        this.accountConcurrentHashMap.putAll(accountList);
    }

    public void getListAllAccounts() {
        System.out.println("______________________________");
        for (BankAccount bankAccount : getAccountConcurrentHashMap().values()) {
            logger.info(bankAccount.toString());
        }
        System.out.println("______________________________");
    }
}
