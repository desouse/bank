package ru.sbertech.bank.entities;

import java.io.Serializable;
import java.math.BigDecimal;

public abstract class BankAccount implements Serializable {

    long numberBankAccount;
    private final String typeBankAccount;
    private final int codeBank;
    private BigDecimal balance;

    BankAccount(String typeBankAccount, int codeBank, double balance) {
        this.typeBankAccount = typeBankAccount;
        this.codeBank = codeBank;
        this.balance = new BigDecimal(balance).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    long getNumberBankAccount() {
        return numberBankAccount;
    }

    public int getCodeBank() {
        return codeBank;
    }

    private String getTypeBankAccount() {
        return typeBankAccount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public boolean doCreditOperation(BigDecimal credit)
    {
        if (credit.compareTo(getBalance()) <= 0)
        {
            balance = balance.subtract(credit);
            return true;
        }
        return false;
    }

    public void doDebitOperation(BigDecimal debit)
    {
        balance = balance.add(debit);
    }

    @Override
    public String toString() {
        return getTypeBankAccount() + "{" +
                "BankAccountID = " + numberBankAccount +
                ", balance = " + balance +
                '}';
    }
}
