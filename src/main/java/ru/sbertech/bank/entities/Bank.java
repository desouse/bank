package ru.sbertech.bank.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class Bank {
    private final static Logger logger = LoggerFactory.getLogger(Bank.class);
    public static BankAccountList bankAccountList = new BankAccountList();
    private int codeBank;
    private String name;

    public Bank(String name, int codeBank) {
        this.name = name;
        this.codeBank = codeBank;
    }

    public int getCodeBank() {
        return codeBank;
    }

    public String getName() {
        return name;
    }

    public static BankAccountList getBankAccountList() {
        return bankAccountList;
    }

    public void saveData() {
        String fileName = getCodeBank() + ".dat";
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName); ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(bankAccountList);
            objectOutputStream.flush();
        } catch (IOException e) {
            logger.error("Error writing to file");
            e.printStackTrace();
        }
        logger.info("Data was successfully saved");
    }

    public void loadData() {
        String fileName = getCodeBank() + ".dat";
        try (FileInputStream fileInputStream = new FileInputStream(fileName); ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            bankAccountList = (BankAccountList) objectInputStream.readObject();
            logger.info("Data was successfully loaded");
        } catch (FileNotFoundException e) {
            logger.error("File not found");
        } catch (IOException | ClassNotFoundException e) {
            logger.error("Error reading from file");
        }
    }

    public synchronized void doInternalTransaction(PaymentDocument document) {
        BankAccount credit = bankAccountList.getAccountConcurrentHashMap().get(document.getCreditAccount());
        BankAccount debit = bankAccountList.getAccountConcurrentHashMap().get(document.getDebitAccount());
        if (credit.doCreditOperation(document.getSum())) {
            debit.doDebitOperation(document.getSum());
        } else {
            logger.error("The operation was failed. The credit bank account does not have enough money");
        }
        logger.info("The operation was successfully completed");
    }
}
